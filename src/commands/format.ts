import * as yaml from "js-yaml";
import { getCulture, parse } from "../common";
import * as markdownTable from "markdown-table";

export var command = "format <input>";
export var describe = "formats the argument into the known formats of the culture";

export var builder = {
    culture: {
        default: process.env.LANGUAGE,
        nargs: 1,
    },
    data: {
        describe: "the directory containing the cultural .json files",
        nargs: 1,
    },
    "input-format": {
        describe: "the culture-specific formatting to parse the input",
        nargs: 1,
    },
    "output-format": {
        describe: "the format to use for the output, otherwise all",
        nargs: 1,
    },
    "output-style": {
        choices: ["bare", "markdown-table"],
        default: "bare",
        describe: "the format of the results",
        nargs: 1,
    },
    type: {
        choices: ["instant", "period"],
        default: "instant",
        nargs: 1,
    },
};

export function handler(argv) {
    // Parse the input.
    const parsed = parse(argv, "input-format", argv.input.toString());

    // We need the culture so we can figure out the formats.
    const culture = getCulture(argv);
    const calendarData = culture.data[argv.type + "s"];
    const formats = calendarData.formats;

    // Go through all the known formats of this culture. Because we like
    // everything ordered, we get the ids and then sort the results.
    let formatIds = [];

    if (argv.outputFormat) {
        formatIds.push(argv.outputFormat);
    } else {
        for (const formatId of culture.getInstantFormats()) {
            formatIds.push(formatId);
        }
    }

    formatIds.sort();

    // Go through the list of formats and build up an array of values.
    var results = [];

    for (const formatId of formatIds) {
        var formatted = culture.format(parsed, formatId);

        results.push([formatId, formatted]);
    }

    // Based on the output, we'll figure out how to format this.
    switch (argv.outputStyle) {
        case "bare":
            for (const r of results) {
                console.log(r[1]);
            }
            break;

        case "markdown-table":
            results.unshift(["format", "results"]);
            console.log(markdownTable(results));
            break;
    }
}
