import * as yaml from "js-yaml";
import { parse } from "../common";

export var command = "parse <input>";
export var describe = "parse the argument and show the resulting parsed output";

export var builder = {
    culture: {
        default: process.env.LANGUAGE,
        nargs: 1,
    },
    data: {
        describe: "the directory containing the cultural .json files",
        nargs: 1,
    },
    format: {
        describe: "the culture-specific formatting to parse the input",
        nargs: 1,
    },
    indent: {
        default: "0",
        describe: "the indention to use for formatting JSON and YAML "
            + "or 't' for tabs",
            nargs: 1,
    },
    json: {
        default: true,
        nargs: 0,
    },
    type: {
        choices: ["instant", "period"],
        default: "instant",
        nargs: 1,
    },
    yaml: {
        default: false,
        nargs: 0,
    },
};

export function handler(argv) {
    // Parse the input.
    const parsed = parse(argv, "format", argv.input.toString());

    // If we aren't mapping BigJS values, replace it.
    if (!argv.bigjs) {
        parsed.julian = Number(parsed.julian);
    }

    // Figure the output formatting.
    let indent: any = argv.indent === "t" ? "\t" : parseInt(argv.indent, 10);

    if (argv.json) {
        console.log(JSON.stringify(parsed, null, indent));
    }

    if (argv.yaml) {
        var yamlIndent = parseInt(indent);

        console.log(yaml.safeDump(parsed, { indent: yamlIndent }));
    }
}
