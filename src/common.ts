import { Culture, PropertyCultureDataProvider } from "mfgames-culture";
import * as cultureData from "mfgames-culture-data";
import * as cultureNode from "mfgames-culture-node";

let currentCulture: Culture = undefined;

export function getCulture(argv: any): any {
    // If we've already loaded the culture, then just return it.
    if (currentCulture) {
        return currentCulture;
    }

    // Set up the provider using the given directory or the built-in method.
    let provider;

    if (argv.data) {
        const dataDirectory = argv.data || cultureData.dataDirectory;
        provider = new cultureNode.DirctoryCultureDataProvider(dataDirectory);
    } else {
        provider = new PropertyCultureDataProvider(cultureData.combined);
    }

    // Get the culture infromation. We can get a "_" instead of "-", so use
    // try both.
    let data;

    try {
        data = provider.getCultureSync(argv.culture.replace("_", "-"));
    } catch (exception) {
        data = provider.getCultureSync(argv.culture.replace("-", "_"));
    }

    // Get the instant or period we want from the results.
    return currentCulture = new Culture(data);
}

export function parse(argv: any, inputFormatKey: string, input: string): any {
    // Get the instant or period we want from the results.
    const culture = getCulture(argv);
    let parsed;

    if (argv.type === "period") {
        parsed = culture.parsePeriod(input, argv[inputFormatKey]);
    } else {
        parsed = culture.parseInstant(input, argv[inputFormatKey]);
    }

    // Return the resulting parsed object.
    return parsed;
}
