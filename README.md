# MfGames Culture (CLI)

Provides a command line interface (CLI) for [MfGames Culture](https://mfgames.com/mfgames-culture).

## License

MIT

## Parsing

The following are examples with `en_US` in the `LANGUAGE` environment.

```shell
$ mfgames-culture parse 7/14/2017
{"julian":2457948.5,"type":"instant","year":2017,"century":20,"millenniumCentury":0,"decade":201,"centuryDecade":1,"centuryYear":17,"decadeYear":7,"millennium":2,"yearDay":194,"yearMonth":6,"monthDay":13,"hour24":0,"meridiem":0,"hour12":0,"hourMinute":0,"minuteSecond":0}
$
```

```shell
$ mfgames-culture parse 7/14/2017 --indent 2
{
  "julian": 2457948.5,
  "type": "instant",
  "year": 2017,
  "century": 20,
  "millenniumCentury": 0,
  "decade": 201,
  "centuryDecade": 1,
  "centuryYear": 17,
  "decadeYear": 7,
  "millennium": 2,
  "yearDay": 194,
  "yearMonth": 6,
  "monthDay": 13,
  "hour24": 0,
  "meridiem": 0,
  "hour12": 0,
  "hourMinute": 0,
  "minuteSecond": 0
}
$
```

```shell
$ mfgames-culture parse 7/14/2017 --no-json --yaml
julian: 2457948.5
type: instant
year: 2017
century: 20
millenniumCentury: 0
decade: 201
centuryDecade: 1
centuryYear: 17
decadeYear: 7
millennium: 2
yearDay: 194
yearMonth: 6
monthDay: 13
hour24: 0
meridiem: 0
hour12: 0
hourMinute: 0
minuteSecond: 0
$
```

### Options

#### --culture

**string**

Default: the process's culture

The culture key to use from inside the data file. For example, `en-US`. Both underscore and dashes are tested.

#### --data

**string**

Default: the built-in `mfgames-culture-data` source.

If this is given, then the culture files will be looked from the given directory instead of using `mfgames-culture-data`.

#### --format

**string**

Default: use the library's heuristics for identifying format

If provided, then the format of the input will be compared only against the given format instead of testing against all valid ones.

#### --indent

**number|'t'**

Default: `0`

If this provided, then it will be used to control how much indention is used for formatting JSON or YAML. If `t` is provided, then JSON will be formatted with tabs.

#### --json, --no-json

**boolean**

Default: true

If true, then JSON output will be generated.

#### --type

**'instant'|'period'**

Default: `instant`

This will determine if the input is treated as an instant or a period.

#### --yaml, --no-yaml

**boolean**

Default: false

If this is true (`--yaml`), then YAML output will be produced. If `--no-json` isn't used, then JSON will *also* be printed to the output.

## Formatting

The following are examples with `en_US` in the `LANGUAGE` environment.

```shell
$ mfgames-culture format 7/14/2017
7/14/2017 0:00 AM
0:00 AM
7/14/2017
7/14/2017 0:00 AM
07/14/2017
07/14/2017 0:00:00 AM
Jul 14, 17
7/14/2017
7/14/2017 0:00 AM
0:00 AM
2017-07-14
2017-07-14 00:00:00
$
```

```shell
$ mfgames-culture format 7/14/2017 --output-format 'YYYY-MM-DD'
2017-07-14
$
```

```shell
$ mfgames-culture format 7/14/2017 --output-style markdown-table
| format                | results               |
| --------------------- | --------------------- |
| default               | 7/14/2017 0:00 AM     |
| h:mm tt               | 0:00 AM               |
| M/D/YYYY              | 7/14/2017             |
| M/D/YYYY h:mm tt      | 7/14/2017 0:00 AM     |
| MM/DD/YYYY            | 07/14/2017            |
| MM/DD/YYYY h:mm:ss tt | 07/14/2017 0:00:00 AM |
| MMM DD, YY            | Jul 14, 17            |
| shortDate             | 7/14/2017             |
| shortDateTime         | 7/14/2017 0:00 AM     |
| shortTime             | 0:00 AM               |
| YYYY-MM-DD            | 2017-07-14            |
| YYYY-MM-DD HH:mm:ss   | 2017-07-14 00:00:00   |
$
```

### Options

#### --culture

**string**

Default: the process's culture

The culture key to use from inside the data file. For example, `en-US`. Both underscore and dashes are tested.

#### --data

**string**

Default: the built-in `mfgames-culture-data` source.

If this is given, then the culture files will be looked from the given directory instead of using `mfgames-culture-data`.

#### --input-format

**string**

Default: use the library's heuristics for identifying format

If provided, then the format of the input will be compared only against the given format instead of testing against all valid ones.

#### --output-format

**string**

Default: show all formats

If provided, then the results will only be formatted with the given format, otherwise a table with all the formats will be printed.

#### --output-style

**'bare'|'markdown-table'**

Default: `bare`

Determines how the results are formatted and displayed. `bare` just lists the formatted results, one on each line. `markdown-table` shows a Markdown table with the format id in the first column and the formatted results in the other.

#### --type

**'instant'|'period'**

Default: `instant`

This will determine if the input is treated as an instant or a period.
